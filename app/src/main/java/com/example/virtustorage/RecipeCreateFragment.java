package com.example.virtustorage;

import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

public class RecipeCreateFragment extends Fragment implements View.OnClickListener ,RatingBar.OnRatingBarChangeListener{

    private String TAG = "R_Create_Frag";

    private Button safe;
    private Button delete;
    private EditText recipe_name;
    private RatingBar ratingBar;

    private EditText searchView;

    private Button add_ingredient;
    private RecyclerView recyclerView;
    private R_Create_Adapter recyclerAdapter;
    private ItemDatabase itemDatabase;
    private ItemDao itemDao;
    private List<Item> items;
    private RecipeDatabase recipeDatabase;
    private Recipe recipe;
    private EditText description;

    private boolean newRecipe;

    //Constructor
    public RecipeCreateFragment (Recipe recipeIN, boolean newRecipeIn)
    {
        recipe = recipeIN;
        newRecipe = newRecipeIn;
        items = new ArrayList<>();
        Log.d(TAG, "RecipeCreateFragment: " + recipe.toString());
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        initDb();
        itemDao = itemDatabase.itemDao();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = layoutInflater.inflate(R.layout.recipe_create_fragment,container,false);

        safe = view.findViewById(R.id.safe_recipe_create);
        safe.setOnClickListener(this);

        delete = view.findViewById(R.id.delete_recipe_create);
        delete.setOnClickListener(this);

        recipe_name = view.findViewById(R.id.recipe_name_create);

        add_ingredient = view.findViewById(R.id.add_ingredient);
        add_ingredient.setOnClickListener(this);

        ratingBar = view.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(this);

        searchView = view.findViewById(R.id.searchIngredient);

        description = view.findViewById(R.id.recipe_description_in);

        convertIngredients();
        Log.d(TAG, "onCreateView: " + recipe.getIngredientList().size());
        if (recipe.getIngredientList().size() > 0)
        {
            Log.d(TAG, "onCreateView: " + items.size());
            setRecyclerView(view);
        }

        displayExistingInformation();

        return view;
    }

    //handle button clicks
    public void onClick(final View view)
    {
        Snackbar snackbar;

        //add-Button
        if (view.getId() == add_ingredient.getId())
        {
            getSearchIngridient(view);
        }
        //safe-Button
        if (view.getId() == safe.getId())
        {
            Log.d(TAG, "onClick: safe");
            getInformation();
            safeRecipeToDb(recipe);

            snackbar = Snackbar.make(view,"Rezept gespeichert!",Snackbar.LENGTH_SHORT);
            snackbar.show();

            closeFragment(view);
        }
        //delete-Button
        else if (view.getId() == delete.getId())
        {
            Log.d(TAG, "onClick: delete");
            snackbar = Snackbar.make(view,"Rezept nicht gespeichert!",Snackbar.LENGTH_SHORT);
            snackbar.show();

            closeFragment(view);
        }
    }

    //handle ratings
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

        if (fromUser) {
            Log.d(TAG, "onRatingChanged: " + rating);
            recipe.setStars(ratingBar.getRating());
            safeRecipeToDb(recipe);
        }
    }

    //creates the RecyclerView
    private void setRecyclerView(View view) {

        recyclerView = view.findViewById(R.id.recycler_view_recipe);

        recyclerAdapter = new R_Create_Adapter(getContext(), items);

        recyclerView.setAdapter(recyclerAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    //brings the user back to the storage list
    private void closeFragment(View view)
    {
        AppCompatActivity activity = (AppCompatActivity) view.getContext();
        Fragment recipeListFragment = new RecipeListFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, recipeListFragment).addToBackStack(null).commit();


    }


    //convert ingredient-list from string to Item
    private void convertIngredients()
    {
        ArrayList<String> ingredientList = recipe.getIngredientList();
        String temp;
        int i = 0;
        while (i < ingredientList.size())
        {
            temp = ingredientList.get(i);
            items.add(itemDao.getItem(temp));
            i++;
        }
    }

    //new recipes are updated and saved to the database
    private void safeRecipeToDb(final Recipe recipe)
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        RecipeDao recipeDao = recipeDatabase.recipeDao();
                        if (newRecipe) {recipeDao.insert(recipe);}
                        else {recipeDao.update(recipe);}
                    }
                }
        ).start();
    }

    //initializes the databank/fetches the date added earlier
    private void initDb()
    {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
        recipeDatabase = RecipeDatabase.getDatabase(getActivity());
    }

    //if item-information already exists, it will now be displayed in the Edit-Text-Boxes
    private void displayExistingInformation()
    {
        if (recipe.getRecipeName() != null)
        {
            recipe_name.setText(recipe.getRecipeName());
        }
        if (recipe.getDescription() != null)
        {
            description.setText(recipe.getDescription());
        }
        if (recipe.getStars() != 0)
        {
            ratingBar.setRating(recipe.getStars());
        }
    }

    private void getInformation()
    {
        if (recipe_name.getText().toString().trim().length() > 0)
        {
            recipe.setRecipeName(recipe_name.getText().toString());
            Log.d(TAG, "getInformation: " + recipe_name.getText());
        }
        if (description.getText().toString().trim().length() > 0)
        {
            recipe.setDescription(description.getText().toString());
        }
    }

    private void getSearchIngridient(View view)
    {
        Snackbar snackbar;

        if (searchView.getText().toString().trim().length() <= 0)
        {
            snackbar = Snackbar.make(view,"Bitte Namen eingeben.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
        else {
            ItemDao itemDao = itemDatabase.itemDao();

            if (searchView.getText().toString().equals(itemDao.findItemName(searchView.getText().toString())))
            {
                Item item = itemDao.getItem(searchView.getText().toString());

                recipe.addIngredient(item);

                safeRecipeToDb(recipe);

                searchView.getText().clear();
                Log.d(TAG, "getSearchIngridient: existing Item used");
            }
            else
            {
                Log.d(TAG, "getSearchIngridient: new Item created");
                Item item = new Item();
                item.setItemName(searchView.getText().toString());
                recipe.addIngredient(item);
                safeItemToDb(item);

                searchView.getText().clear();
            }
            snackbar = Snackbar.make(view, "Zutat hinzugefügt!", Snackbar.LENGTH_SHORT);
            snackbar.show();
            //refresh(view);
        }
    }

    private void safeItemToDb(final Item item)
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.insert(item);

                        List<Item> itemList = itemDao.getAllWords();
                        ArrayList<Item> itemArrayList = new ArrayList<>(itemList);

                        for (Item tempItem : itemArrayList) {
                            Log.i("ShoppingFragment", tempItem.toString());
                        }
                    }
                }
        ).start();
    }

    private void refresh(View view)
    {
        convertIngredients();
        setRecyclerView(view);
    }

}
