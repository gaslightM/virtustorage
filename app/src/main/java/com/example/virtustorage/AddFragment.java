package com.example.virtustorage;


import android.Manifest;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.Fragment;
import android.widget.Toolbar;

import com.google.zxing.Result;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.core.BarcodeScannerView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static com.example.virtustorage.MainActivity.REQUEST_CODE_SCANNEDNAME;

public class AddFragment extends Fragment implements View.OnClickListener
{
    private ImageButton openBarcodeScannerButton;
    private Button submitItemButton;
    private ItemDatabase itemDatabase;
    private EditText itemName;
    private EditText itemAmount;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View addView = inflater.inflate(R.layout.add_fragment, container, false);

        openBarcodeScannerButton = addView.findViewById(R.id.openBarcodeScannerButton);
        submitItemButton = addView.findViewById(R.id.submitItemButton);
        itemName = addView.findViewById(R.id.itemName);
        itemAmount = addView.findViewById(R.id.itemAmount);

        openBarcodeScannerButton.setOnClickListener(this);
        submitItemButton.setOnClickListener(this);

        initDb();

        return addView;
    }

    //Setzt den Namen in das Editextfeld
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_SCANNEDNAME)
        {
            if(resultCode == RESULT_OK)
            {
                Bundle results = data.getExtras();

                String scannedName = results.getString("SCANNED_NAME");

                itemName.setText(scannedName);
            }
        }
    }

    private void initDb() {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.openBarcodeScannerButton:
                Intent intent = new Intent(getActivity(), BarcodeScannerActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SCANNEDNAME);
                break;
            case R.id.submitItemButton:

                if(itemName.getText().toString().trim().length() <= 0)
                {
                    Toast.makeText(getActivity(), "Please enter a name", Toast.LENGTH_SHORT).show();
                    break;
                }
                else if(itemAmount.getText().toString().trim().length() <= 0)
                {
                    Toast.makeText(getActivity(), "Please enter a quantity", Toast.LENGTH_SHORT).show();
                    break;
                }
                else if(Integer.parseInt(itemAmount.getText().toString()) < 0)
                {
                    Toast.makeText(getActivity(), "Invalid amount", Toast.LENGTH_SHORT).show();
                    break;
                }

                ItemDao itemDao = itemDatabase.itemDao();

                if(itemName.getText().toString().equals(itemDao.findItemName(itemName.getText().toString())))
                {
                    Item item = itemDao.getItem(itemName.getText().toString());

                    item.updateAmount(Integer.parseInt(itemAmount.getText().toString()));

                    updateItemDb(item);

                    Toast.makeText(getActivity(), "Amount has been updated", Toast.LENGTH_SHORT).show();

                    itemName.getText().clear();
                    itemAmount.getText().clear();

                }
                else
                {
                    safeItemToDb();

                    Toast.makeText(getActivity(), "Item added to Storagelist", Toast.LENGTH_SHORT).show();

                    itemName.getText().clear();
                    itemAmount.getText().clear();
                }

                break;
        }
    }

    private void updateItemDb(final Item item)
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

    private void safeItemToDb()
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Item item = new Item();
                        item.setItemName(itemName.getText().toString());
                        item.setItemAmount(Integer.parseInt(itemAmount.getText().toString()));

                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.insert(item);

                        List<Item> itemList = itemDao.getAllWords();
                        ArrayList<Item> itemArrayList = new ArrayList<>(itemList);

                        for (Item tempItem : itemArrayList) {
                            Log.i("AddFragment", tempItem.toString());
                        }
                    }
                }
        ).start();
    }
}
