package com.example.virtustorage;


import android.app.usage.UsageEvents;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ItemDao {
    @Query("SELECT * FROM item")
    List<Item> getAll();

    @Query("SELECT * FROM  item WHERE item_amount>0")
    List<Item> getAllExisting();

    @Query("SELECT * FROM  item WHERE shop_amount>0")
    List<Item> getShopping();

    @Query("SELECT * FROM item WHERE dietitian = 1")
    List<Item> getDietitianItems();

    @Query("SELECT * FROM item WHERE item_name = :name LIMIT 1")
    List<Item> getItemName(String name);

    @Query("SELECT item_name FROM item WHERE item_name = :name LIMIT 1")
    String findItemName(String name);

    @Query("SELECT * FROM item WHERE item_name = :name LIMIT 1")
    Item getItem(String name);

    @Query("SELECT * FROM item WHERE id = :ID")
    Item getItem(int ID);

    @Insert
    void insert(Item item);

    @Update
    void update(Item item);

    @Query("DELETE FROM item")
    void deleteAll();

    @Query("SELECT * from item ORDER BY item_name ASC")
    List<Item> getAllWords();

    @Query("SELECT * FROM Item WHERE item_name LIKE '%' || :categoryName|| '%'")
    LiveData<List<Item>> getItemList(String categoryName);


}

