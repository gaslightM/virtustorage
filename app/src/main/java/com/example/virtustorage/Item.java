package com.example.virtustorage;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Calendar;

import static android.support.constraint.Constraints.TAG;

@Entity(tableName = "item")
public class Item {

    public int getId() {
        return id;
    }

    @NonNull
    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "item_name")
    private String itemName;

    @ColumnInfo(name = "item_amount")
    private Integer itemAmount=0;

    @ColumnInfo(name = "use_by_day")
    private int use_by_DAY;

    @ColumnInfo(name = "use_by_month")
    private int use_by_MONTH;

    @ColumnInfo(name = "use_by_year")
    private int use_by_YEAR;

    @ColumnInfo(name = "min_amount")
    private Integer minAmount = 0;

    @ColumnInfo(name = "calories")
    private Integer calories = 0;

    @ColumnInfo(name = "weight")
    private Integer weight;

    @ColumnInfo(name = "dietitian")
    private boolean in_dietitian;

    @ColumnInfo(name = "shop_amount")
    private Integer shop_amount = 0;

    @ColumnInfo(name = "checkbox")
    private boolean checkbox;

    public Item() {
    }

    public Item(String itemName, Integer itemAmount) {
        this.itemName = itemName;
        this.itemAmount = itemAmount;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return this.itemName;
    }

    public void setItemAmount(Integer itemAmount) {
        this.itemAmount = itemAmount;
    }

    public Integer getItemAmount() {
        return this.itemAmount;
    }

    public void setShop_amount(Integer shop_amount) {
        this.shop_amount = shop_amount;
    }

    public Integer getShop_amount() {
        return this.shop_amount;
    }

    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    public Integer getMinAmount() {
        return this.minAmount;
    }

    public void setUse_by_DAY(int use_by_DAY) {
        this.use_by_DAY = use_by_DAY;
    }

    public int getUse_by_DAY() {
        return use_by_DAY;
    }

    public void setUse_by_MONTH(int use_by_MONTH) {
        this.use_by_MONTH = use_by_MONTH;
    }

    public int getUse_by_MONTH() {
        return use_by_MONTH;
    }

    public void setUse_by_YEAR(int use_by_YEAR) {
        this.use_by_YEAR = use_by_YEAR;
    }

    public int getUse_by_YEAR() {
        return use_by_YEAR;
    }

    public void setUse_By_Date(int day, int month, int year) {
        setUse_by_DAY(day);
        setUse_by_MONTH(month);
        setUse_by_YEAR(year);
    }

    public Calendar getUse_By_Date() {
        Calendar use_by = Calendar.getInstance();
        use_by.set(this.use_by_YEAR, this.use_by_MONTH, this.use_by_DAY);

        return use_by;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getCalories() {
        return this.calories;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return this.weight;
    }

    public void setIn_dietitian(boolean in_dietitian) {
        this.in_dietitian = in_dietitian;
    }

    public boolean getIn_dietitian() {
        return in_dietitian;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void decreaseAmount() {
        this.itemAmount = this.itemAmount - 1;
    }

    public void updateAmount(Integer itemAmount) {
        this.itemAmount += itemAmount;
    }

    public void updateShopAmount(Integer shop_amount) {
        this.shop_amount += shop_amount;
    }

    public void increaseShopAmount() {
        this.shop_amount += 1;
    }

    @Override
    public String toString() {
        return "id: " + this.id + " Name: " + this.itemName + " Anzahl: " + this.itemAmount
                + " haltbar bis: " + this.use_by_DAY + "/" + (this.use_by_MONTH + 1) + "/" + this.use_by_YEAR
                + " Mindestanzahl: " + minAmount
                + " Kalorien: " + this.calories + "kcal"
                + " Gewicht des Inhalts: " + this.weight + "g " + this.in_dietitian + "shop: " + this.shop_amount;
    }
}