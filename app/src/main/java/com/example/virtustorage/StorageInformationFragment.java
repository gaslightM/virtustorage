package com.example.virtustorage;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class StorageInformationFragment extends Fragment implements View.OnClickListener {

    private String TAG = "S_Info_Frag";

    private Item item;
    private ItemDatabase itemDatabase;
    private Button safe;
    private Button delete;
    private TextView product_name;
    private Button use_by;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private EditText min_amount;
    private EditText calories;
    private EditText weight;

    //Konstruktor
    public StorageInformationFragment(Item itemIN)
    {
        item = itemIN;
        Log.d(TAG, "StorageInformationFragment: " + item.toString());
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: started!");

        initDb();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = layoutInflater.inflate(R.layout.storage_information_fragment,container,false);
        Log.d(TAG, "onCreateView: started!");

        safe = view.findViewById(R.id.safe_product_info);
        safe.setOnClickListener(this);

        delete = view.findViewById(R.id.delete_product_info);
        delete.setOnClickListener(this);

        product_name = view.findViewById(R.id.product_name_info);
        product_name.setText(item.getItemName());

        use_by = view.findViewById(R.id.use_by_date_in);
        use_by.setOnClickListener(this);

        min_amount = view.findViewById(R.id.min_amount_in);
        calories = view.findViewById(R.id.calories_in);
        weight = view.findViewById(R.id.weight_in);

        initDatePicker();
        displayExistingInformation();

        return view;
    }

    @Override
    public void onClick(final View view)
    {
        Snackbar snackbar;

        if (view.getId() == safe.getId())
        {
            Log.d(TAG, "onClick: safe");
            getInformation();
            safeItemToDb(item);

            snackbar = Snackbar.make(view,"Änderungen gespeichert!",Snackbar.LENGTH_SHORT);
            snackbar.show();

            closeFragment(view);
        }
        else if (view.getId() == delete.getId())
        {
            Log.d(TAG, "onClick: delete");
            snackbar = Snackbar.make(view,"Änderungen nicht gespeichert!",Snackbar.LENGTH_SHORT);
            snackbar.show();

            closeFragment(view);
        }
        else if (view.getId() == use_by.getId())
        {
            initDatePickerDialog(view);
        }
    }

    //fetches the information written into the EditText-Fields
    private void getInformation()
    {
        Log.d(TAG, "getInformation: started");

        if (min_amount.getText().toString().trim().length() > 0)
        {
            item.setMinAmount(Integer.parseInt(min_amount.getText().toString()));
            Log.d(TAG, "getInformation: minAmount = " + min_amount.getText());
        }
        if (calories.getText().toString().trim().length() > 0)
        {
            item.setCalories(Integer.parseInt(calories.getText().toString()));
            Log.d(TAG, "getInformation: calories = " + calories.getText());
        }
        if (weight.getText().toString().trim().length() > 0)
        {
            item.setWeight(Integer.parseInt(weight.getText().toString()));
            Log.d(TAG, "getInformation: weight = " + weight.getText());
        }

        Log.d(TAG, "getInformation: finished");
    }

    //new item values are updated to the databank
    private void safeItemToDb(final Item item)
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

    //initializes the databank/fetches the date added earlier
    private void initDb()
    {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
    }

    //brings the user back to the storage list
    private void closeFragment(View view)
    {
        AppCompatActivity activity = (AppCompatActivity) view.getContext();
        Fragment storageListFragment = new StorageListFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, storageListFragment).addToBackStack(null).commit();


    }

    //initializes the DatePickerDialog
    private void initDatePickerDialog(View view)
    {
        //Calendar window will pop up
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                view.getContext(),
                android.R.style.Theme_Holo_Light_Dialog,
                dateSetListener,
                year,month,day);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    //initializes the dataSetListener
    private void initDatePicker()
    {
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {

                item.setUse_By_Date(day,month,year);
                month = month+1;
                Log.d(TAG, "onDateSet: dd/mm/yyyy: " + day + "/" + month + "/" + year);
            }
        };
    }

    //if item-information already exists, it will now be displayed in the Edit-Text-Boxes
    private void displayExistingInformation()
    {
        if (item.getMinAmount() != null)
        {
            min_amount.setText(Integer.toString(item.getMinAmount()));
        }
        if (item.getCalories() != null)
        {
            calories.setText(Integer.toString(item.getCalories()));
        }
        if (item.getWeight() != null)
        {
            weight.setText(Integer.toString(item.getWeight()));
        }
    }
}
