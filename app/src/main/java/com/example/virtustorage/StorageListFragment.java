package com.example.virtustorage;

import android.content.Context;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;


import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class StorageListFragment extends Fragment {

    public List<Item> items;

    private String TAG = "Storage_Fragment ";

    private RecyclerView recyclerView;
    private S_ListAdapter listAdapter;
    private ItemDatabase itemDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDb();
        itemsInit();
        Log.d(TAG, "onCreate: ItemsInit.execute()");

        Log.d(TAG, "onCreate: finished");

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: before inflate");
        View view = layoutInflater.inflate(R.layout.storage_list_fragment, container, false);
        Log.d(TAG, "onCreateView: inflate finished");

        setRecyclerView(view);

        return view;
    }

    //creates the RecyclerView
    private void setRecyclerView(View view) {
        Log.d(TAG, "setRecyclerView: started");
        recyclerView = view.findViewById(R.id.recycler_view_storage);

        //new S_ListAdapter is created.
        //The second parameter is the Item-list/ the information to be shown in the storage-list
        listAdapter = new S_ListAdapter(getContext(), items); //new version with item-list as parameter

        recyclerView.setAdapter(listAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        swipeActions(recyclerView);


        Log.d(TAG, "setRecyclerView: finished");
    }

    //Implementation and Handling of SwipeActions of StorageList-Items
    private void swipeActions(final RecyclerView recyclerView) {

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            //this is for drag&drop stuff, which we do not use
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            //here swipes are handled
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //here we get to see which item was swiped
                Item item = listAdapter.getItem(viewHolder.getAdapterPosition());

                //Swipe to the Left
                if (direction == ItemTouchHelper.LEFT) {
                    Log.d(TAG, "onSwiped: swipe left " + item.toString());
                    item.decreaseAmount();
                    safeItemToDb(item);

                    //snackbar appears and tells the user what happened
                    Snackbar snackbar = Snackbar
                            .make(recyclerView, "Produktanzahl verringert!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    minAmountWarning(item);
                    refresh();
                }
                //Swipe to the Right
                else if (direction == ItemTouchHelper.RIGHT) {

                    Log.d(TAG, "onSwiped: swipe right" + item.toString());
                    Snackbar snackbar = Snackbar
                            .make(recyclerView, "Zur Einkaufsliste hinzugefügt", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    item.increaseShopAmount();
                    safeItemToDb(item);
                    refresh();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    //refresh the adapter
    private void refresh() {
        itemsInit();
        listAdapter = new S_ListAdapter(getContext(), items);
        recyclerView.setAdapter(listAdapter);
    }

    //executes ItemsInit-Class
    private void itemsInit() {
        new ItemsInit().execute();
    }

    //initializes the databank/fetches the data added earlier
    private void initDb() {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
    }

    //new item values are updated to the databank
    private void safeItemToDb(final Item item) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

    //init Item-List 'items' with itemDatabase
    // but only Items with Item_Amount > 0 are displayed. So we just import those.
    //therefor we use AsyncTasks because
    // 'database cannot access on the main thread since it may potentially lock the UI for a long period of time'
    private class ItemsInit extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            items = itemDatabase.itemDao().getAllExisting();
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: ItemsInit " + items.toString());
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

    }

    private void minAmountWarning(Item item)
    {
        if (item.getMinAmount() > 0 && item.getMinAmount() >= item.getItemAmount())
        {
            Snackbar snackbar = Snackbar.make(recyclerView,"Mindestmenge erreicht!", Snackbar.LENGTH_LONG)
                    .setAction("wieder einkaufen?",new addToShoppingList(item));
            snackbar.show();
        }
    }

    private class addToShoppingList implements View.OnClickListener
    {
        Item item;

        public addToShoppingList (Item itemIN)
        {
            item = itemIN;
        }

        @Override
        public void onClick(View v) {

            item.setShop_amount((item.getMinAmount()-item.getItemAmount()+1)+item.getShop_amount());
            safeItemToDb(item);
        }
    }

}


