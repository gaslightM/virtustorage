package com.example.virtustorage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Dietitian_RecyclerAdapter extends RecyclerView.Adapter<Dietitian_RecyclerAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<Item> itemList;


    public Dietitian_RecyclerAdapter(Context context, List<Item> itemName) {
        inflater = LayoutInflater.from(context);
        itemList = itemName;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View itemView = inflater.inflate(R.layout.dietitian_recycler_card, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Item item = itemList.get(position);
        String current_product = item.getItemName();
        Integer current_calories = item.getCalories();

        viewHolder.item = item;
        viewHolder.product_name.setText(current_product);
        viewHolder.calories.setText(current_calories.toString() + " kcal");
    }

    public Item getItem(int position) {
        return itemList.get(position);
    }

    public int getItemCount() {
        return itemList.size();
    }

    public void removeItem(int position) {
        itemList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Item item;
        private View view;
        public TextView product_name;
        public TextView calories;

        public ViewHolder(@NonNull final View viewIN) {
            super(viewIN);
            view = viewIN;

            product_name = view.findViewById(R.id.product_name);
            calories = view.findViewById(R.id.calories);

        }

    }
}
