package com.example.virtustorage;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "recipe")
public class Recipe {

    public int getId()
    {
        return id;
    }

    @NonNull
    public void setId(int id)
    {
        this.id = id;
    }

    @PrimaryKey (autoGenerate = true)
    private int id;

    @ColumnInfo (name = "recipe_name")
    private String recipeName;

    @ColumnInfo (name = "ingredients")
    private ArrayList<String> ingredientList = new ArrayList<>();

    @ColumnInfo (name = "stars")
    private float stars;

    @ColumnInfo (name = "description")
    private String description;




    public Recipe ()
    {

    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeName() {
        return recipeName;
    }


    public void setIngredientList(ArrayList<String> ingredientList)
    {
        this.ingredientList = ingredientList;
    }

    public void addIngredient (Item ingredient){
        this.ingredientList.add(ingredient.getItemName());
    }

    public ArrayList<String> getIngredientList() {

        return ingredientList;
    }

    public void setStars(float stars) {
        this.stars = stars;
    }

    public float getStars() {
        return stars;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    @Override
    public String toString()
    {
        return  "id: " + this.id
                + " Rezept: " + recipeName
                + " Stars: " + stars
                + " Decr: " + description
                + " Anzahl Zutaten: " + ingredientList.size();
    }
}

