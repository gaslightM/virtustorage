package com.example.virtustorage;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private final String TAG = "Main_Log";
    private final String BackStack = "backstack";
    private static final int REQUEST_CODE = 1;
    public static final int REQUEST_CODE_SCANNEDNAME = 2;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new StorageListFragment());
        verifyPermissions();
    }

    //Ruft die onActivityResult Methode des Fragmenst auf
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: asking user for permissions");
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[2]) == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(MainActivity.this,
                    permissions,
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        verifyPermissions();
    }

    @Override
    public void onBackPressed() {
        if (fragment instanceof StorageListFragment) {
            //Wenn das Fragment bereits das StorageFragment ist, wird die App geschlossen
            super.onBackPressed();
        } else {
            new StorageListFragment();
            BottomNavigationView navigation = findViewById(R.id.navigation);
            navigation.setSelectedItemId(R.id.navigation_storage);
        }

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            // Add the fragment to the 'container' FrameLayout
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(BackStack).commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_add:
                // Create a new Fragment
                fragment = new AddFragment();
                break;

            case R.id.navigation_dietitian:
                // Create a new Fragment
                fragment = new DietitianFragment();
                break;

            case R.id.navigation_recipe:
                // Create a new Fragment
                fragment = new RecipeListFragment();
                break;

            case R.id.navigation_shopping:
                // Create a new Fragment
                fragment = new ShoppingListFragment();
                break;

            case R.id.navigation_storage:
                // Create a new Fragment
                fragment = new StorageListFragment();
                break;

        }

        fragment.setArguments(getIntent().getExtras());

        return loadFragment(fragment);
    }

}
