package com.example.virtustorage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class RecipeListFragment extends Fragment {

    private String TAG = "Recipe";

    public List<Recipe> recipes;

    private RecyclerView recyclerView;
    private R_ListAdapter listAdapter;
    private RecipeDatabase recipeDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initDb();
        recipesInit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = layoutInflater.inflate(R.layout.recipe_fragment, container, false);

        setRecyclerView(view);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Recipe recipe = new Recipe();

                openRecipeCreator(view, recipe);
                /*
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
            }
        });

        return view;

    }
    
    //initializes the RecyclerView
    private void setRecyclerView(View view)
    {
        recyclerView = view.findViewById(R.id.recycler_view_recipe);
        
        listAdapter = new R_ListAdapter(getContext(), recipes);
        
        recyclerView.setAdapter(listAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        Log.d(TAG, "setRecyclerView: created!");
    }

    //initializes the database/feches the data added earlier
    private void initDb()
    {
        recipeDatabase = RecipeDatabase.getDatabase(getActivity());
    }

    //executes RecipesInit-Class
    private void recipesInit()
    {
        new RecipesInit().execute();
    }

    //this works like the ItemsInit-Class in StorageListFragment.java
    private class RecipesInit extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            recipes  = recipeDatabase.recipeDao().getAll();
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: ItemsInit " + recipes.toString());
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            return null;
        }

    }

    //opens RecipeCreateFragment
    private void openRecipeCreator(View view, Recipe recipe)
    {
        AppCompatActivity activity = (AppCompatActivity) view.getContext();
        Fragment recipeCreateFragment = new RecipeCreateFragment(recipe, true);
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, recipeCreateFragment).addToBackStack(null).commit();
    }

    //refresh the adapter
    private void refresh() {
        recipesInit();
        listAdapter = new R_ListAdapter(getContext(), recipes);
        recyclerView.setAdapter(listAdapter);
    }



}
