package com.example.virtustorage;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

@Database(entities = {Recipe.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class RecipeDatabase extends RoomDatabase {
    public abstract RecipeDao recipeDao();

    private static volatile RecipeDatabase INSTANCE = null;


    static RecipeDatabase getDatabase(Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (RecipeDatabase.class)
            {
                if (INSTANCE == null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RecipeDatabase.class, "recipe_db")
                            .allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }


}
