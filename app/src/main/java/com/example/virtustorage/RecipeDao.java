package com.example.virtustorage;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface RecipeDao {
    @Query("SELECT * FROM recipe")
    List<Recipe> getAll();

    @Query("SELECT recipe_name FROM recipe WHERE recipe_name = :name LIMIT 1")
    String findRecipeName(String name);

    @Insert
    void insert(Recipe recipe);

    @Update
    void update(Recipe recipe);

    @Query("DELETE FROM recipe")
    void deleteAll();
}
