package com.example.virtustorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class R_ListAdapter extends RecyclerView.Adapter<R_ListAdapter.ViewHolder> {

    private String TAG = "recipeadapter";

    private List<Recipe> recipeList;
    private LayoutInflater inflater;

    //Constructor
    public R_ListAdapter(Context context, List<Recipe> recipes)
    {
        inflater = LayoutInflater.from(context);
        recipeList = recipes;
        Log.d(TAG, "S_ListAdapter: recipeList in adapter initialized "+ recipeList.toString());
    }

    //Create new views in case there are no existing ones left
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType)
    {
        View recipeView = inflater.inflate(R.layout.recipe_card, parent, false);
        return new ViewHolder(recipeView);
    }

    //data is bound to the views
    @Override
    public void onBindViewHolder (final ViewHolder viewHolder, int position)
    {
        Recipe recipe = recipeList.get(position);
        String current_name = recipe.getRecipeName();

        viewHolder.recipe = recipe;
        viewHolder.recipe_name.setText(current_name);
    }

    //returns Recipe-Object @ given position
    public Recipe getRecipe(int position)
    {
        return recipeList.get(position);
    }

    //returns size of recipeList
    public  int getItemCount()
    {
        return recipeList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private String TAG = "Recipe_Card";

        private Recipe recipe;
        private View view;

        public TextView recipe_name;
        public LinearLayout click;

        //Constructor
        public ViewHolder(@NonNull final View viewIN)
        {
            super(viewIN);
            view = viewIN;

            recipe_name = view.findViewById(R.id.recipe_name);

            click = view.findViewById(R.id.recipe_card_click);
            click.setOnClickListener(this);
        }

        public void onClick(final View view)
        {
            openRecipeCreator(view, recipe);
        }

        //opens RecipeCreateFragment
        private void openRecipeCreator(View view, Recipe recipe)
        {
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            Fragment recipeCreateFragment = new RecipeCreateFragment(recipe, false);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, recipeCreateFragment).addToBackStack(null).commit();
        }

    }
}
