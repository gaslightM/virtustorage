package com.example.virtustorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

public class Shop_ListAdapter extends RecyclerView.Adapter<Shop_ListAdapter.ViewHolder>
{

    private String TAG = "listadapter";
    private int helpamount=0;
    private ItemDatabase itemDatabase;
    private List<Item> itemList;
    private LayoutInflater inflater;

    private SparseBooleanArray mItemStateArray = new SparseBooleanArray();

    //Konstruktor: context ist Referenz auf das ShoppingListFragment.
    public Shop_ListAdapter(Context context, List<Item> items)
    {
        inflater = LayoutInflater.from(context);
        itemList = items;
        initDb();
        Log.d(TAG, "Shop_ListAdapter: itemList in adapter initialized " + itemList.toString());
    }

    //Erstellt neue Views, falls keine existieren, die wieder hergenommen werden können.
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType)
    {
        View itemView = inflater.inflate(R.layout.shopping_card, parent,false);
        return new ViewHolder(itemView);
    }

    //Hier werden neue Daten an alte Views gebunden, damit sie wieder hergenommen werden können. -> Recycle
    @Override
    public void onBindViewHolder (final ViewHolder viewHolder, int position)
    {

        Item item = itemList.get(position);
        String current_product = item.getItemName();
        String current_description = "Zu kaufen: " + item.getShop_amount();

        viewHolder.item = item;
        viewHolder.product_name.setText(current_product);
        viewHolder.description.setText(current_description);

        if (!mItemStateArray.get(position, false)) {
            viewHolder.mCheckBox.setChecked(false);
        } else {
            viewHolder.mCheckBox.setChecked(true);
        }
    }

    //returns size of the itemList of the recyclerview
    public int getItemCount()
    {
        return itemList.size();
    }

    private void initDb() {
        itemDatabase = ItemDatabase.getDatabase(inflater.getContext());
    }

    private void safeItemToDb(final Item item) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private Item item;
        private View view;
        public TextView product_name;
        public TextView description;
        private String TAG = "Shopping_card";

        final CheckBox mCheckBox;

        //Konstruktor: hier werden die erstellten Text-Views mit den XML-Text-Views verbunden per ID.
        public ViewHolder(@NonNull final View viewIN)
        {
            super(viewIN);
            view = viewIN;

            product_name = view.findViewById(R.id.product_name);
            description = view.findViewById(R.id.description);

            mCheckBox = view.findViewById(R.id.shoppingcheckbox);
            mCheckBox.setOnClickListener(this);
        }

        @Override
        public void onClick(final View v) {

            final Boolean mCheckboxState = mCheckBox.isChecked();

            int adapterPosition = getAdapterPosition();
            mItemStateArray.put(adapterPosition, mCheckboxState);

            //Toast.makeText(v.getContext(),
                    //String.format(Locale.GERMAN, "Position: %d is checked: %s", getLayoutPosition(), mCheckboxState),
                    //Toast.LENGTH_SHORT)
                    //.show();

            if(mCheckboxState==true){
                Toast.makeText(v.getContext(),
                        String.format(Locale.GERMAN, "Added to Storage_List with amount %d", item.getShop_amount()), Toast.LENGTH_SHORT).show();
                helpamount=item.getShop_amount();
                item.updateAmount(item.getShop_amount());
                item.setShop_amount(0);
                safeItemToDb(item);
            }
            if(mCheckboxState==false){
                Toast.makeText(v.getContext(),
                        String.format(Locale.GERMAN, "Amount %d Deleted from Storage_List", helpamount), Toast.LENGTH_SHORT).show();

                item.updateAmount(-helpamount);
                item.setShop_amount(helpamount);
                safeItemToDb(item);
            }
        }

    }
}

