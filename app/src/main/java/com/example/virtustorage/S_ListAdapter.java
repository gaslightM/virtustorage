package com.example.virtustorage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class S_ListAdapter extends RecyclerView.Adapter<S_ListAdapter.ViewHolder>
{

    private String TAG = "listadapter";

    private List<Item> itemList;
    private LayoutInflater inflater;

    //Konstruktor: context ist Referenz auf das Storage_list_fragment.
    public S_ListAdapter(Context context, List<Item> items)
    {
        inflater = LayoutInflater.from(context);
        itemList = items;
        Log.d(TAG, "S_ListAdapter: itemList in adapter initialized " + itemList.toString());
    }

    //Erstellt neue Views, falls keine existieren, die wieder hergenommen werden können.
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType)
    {
        View itemView = inflater.inflate(R.layout.storage_card, parent,false);
        return new ViewHolder(itemView);
    }

    //Hier werden neue Daten an alte Views gebunden, damit sie wieder hergenommen werden können. -> Recycle
    @Override
    public void onBindViewHolder (final ViewHolder viewHolder, int position)
    {
        Item item = itemList.get(position);
        String current_product = item.getItemName();
        String current_description = "Anzahl: " + item.getItemAmount();
        String current_useByDate = item.getUse_by_DAY() + "."
                + (item.getUse_by_MONTH()+1) + "."
                + item.getUse_by_YEAR();
        Log.d(TAG, "onBindViewHolder: " + current_useByDate);


        viewHolder.item = item;
        viewHolder.product_name.setText(current_product);
        viewHolder.description.setText(current_description);
        viewHolder.use_by_date.setText(current_useByDate);
    }

    //returns Item-Object @ given position
    public Item getItem(int position)
    {
        return itemList.get(position);
    }

    //returns size of the itemList of the recyclerview
    public int getItemCount()
    {
        return itemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener
    {
        private Item item;
        private View view;
        public TextView product_name;
        public TextView description;
        public TextView use_by_date;
        public ConstraintLayout longClick;
        private String TAG = "Storage_card";

        //Konstruktor: hier werden die erstellten Text-Views mit den XML-Text-Views verbunden per ID.
        public ViewHolder(@NonNull final View viewIN)
        {
            super(viewIN);
            view = viewIN;

            product_name = view.findViewById(R.id.product_name);
            description = view.findViewById(R.id.description);
            use_by_date = view.findViewById(R.id.date);

            longClick = view.findViewById(R.id.product_long_click);
            longClick.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(final View view)
        {

            Log.d(TAG, "onLongClick: longClick on Item: " + item.toString());
            openInformationEditor();
            return true;
        }

        //hier wird das Fragment für den ItemEditor geöffnet.
        private void openInformationEditor()
        {
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            Fragment storageInformationFragment = new StorageInformationFragment(item);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, storageInformationFragment).addToBackStack(null).commit();

        }
    }
}
