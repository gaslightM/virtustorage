package com.example.virtustorage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class Dietitian_ListAdapter extends BaseAdapter {
    private List<Item> itemList;
    private LayoutInflater inflater;


    public Dietitian_ListAdapter(Context context, List<Item> items) {
        inflater = LayoutInflater.from(context);
        itemList = items;
    }

    public class ViewHolder {
        TextView product_name;
    }

    //returns size of the itemList of the recyclerview
    @Override
    public int getCount() {
        return itemList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    //returns Item-Object @ given position
    @Override
    public Item getItem(int position) {
        return itemList.get(position);
    }


    public View getView(final int position, View view, ViewGroup parent) {

        final ViewHolder holder;

        if (view == null) {

            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dietitian_list_card, parent, false);

            // Locate the TextViews in listview_item.xml
            holder.product_name = view.findViewById(R.id.product_name);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();

        }

        // Set the results into TextViews
        holder.product_name.setText(itemList.get(position).getItemName());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();

        if (charText.length() == 0) {
            itemList.clear();
            notifyDataSetChanged();
        } else {
            for (Item wp : itemList) {
                if (wp.getItemName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
