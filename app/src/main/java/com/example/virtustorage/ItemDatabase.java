package com.example.virtustorage;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;

@Database(entities = {Item.class}, version = 1)
public abstract class ItemDatabase extends RoomDatabase {
    public abstract ItemDao itemDao();

    private static volatile ItemDatabase INSTANCE = null;


    static ItemDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (ItemDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ItemDatabase.class, "item_db")
                            .allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }

    static public ItemDao getItemDAO(Context context) {
        return getDatabase(context).itemDao();
    }

    static public LiveData<List<Item>> getItemListInfo(Context context, String query) {
        return getItemDAO(context).getItemList(query);
    }


}
