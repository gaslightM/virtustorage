package com.example.virtustorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class R_Create_Adapter extends RecyclerView.Adapter<R_Create_Adapter.ViewHolder>{

    private String TAG = "rCreate_adapter";

    private List<Item> itemList;
    private LayoutInflater inflater;

    //Constructor
    public R_Create_Adapter (Context context, List<Item> items)
    {
        inflater = LayoutInflater.from(context);
        itemList = items;
        Log.d(TAG, "R_Create_Adapter: created");
    }

    //Create new views in case there are no existing ones left
    @NonNull
    @Override
    public R_Create_Adapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType)
    {
        View recipeView = inflater.inflate(R.layout.dietitian_list_card, parent, false);

        return new R_Create_Adapter.ViewHolder(recipeView);
    }

    //data is bound to the views
    @Override
    public void onBindViewHolder (final R_Create_Adapter.ViewHolder viewHolder, int position)
    {
        Item item = itemList.get(position);
        String current_name = item.getItemName();

        viewHolder.item_name.setText(" - " +current_name);
    }

    //returns Recipe-Object @ given position
    public Item getItem(int position)
    {
        return itemList.get(position);
    }

    //returns size of recipeList
    public  int getItemCount()
    {
        return itemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView item_name;

        //Constructor
        public ViewHolder (@NonNull final View viewIN)
        {
            super(viewIN);

            item_name = viewIN.findViewById(R.id.product_name);
        }
    }
}
