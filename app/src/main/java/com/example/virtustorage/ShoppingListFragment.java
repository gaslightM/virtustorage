package com.example.virtustorage;

import android.content.Context;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class ShoppingListFragment extends Fragment implements View.OnClickListener
{

    public List<Item> items;

    private String TAG = "Shopping_Fragment ";

    private RecyclerView recyclerView;
    private Shop_ListAdapter listAdapter;
    private ItemDatabase itemDatabase;
    private Button okButton;
    private Button submitbutton;
    private EditText shopitemName;
    private EditText shopitemAmount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDb();
        itemsInit();
        Log.d(TAG, "onCreate: ItemsInit.execute()");

        Log.d(TAG, "onCreate: finished");

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: before inflate");
        View view = layoutInflater.inflate(R.layout.shopping_list_fragment, container, false);
        Log.d(TAG, "onCreateView: inflate finished");

        setRecyclerView(view);

        return view;
    }

    //creates the RecyclerView
    private void setRecyclerView(View view) {
        Log.d(TAG, "setRecyclerView: started");
        recyclerView = view.findViewById(R.id.recycler_view_shopping);

        //new Shop_ListAdapter is created.
        //The second parameter is the Item-list/ the information to be shown in the shopping-list
        listAdapter = new Shop_ListAdapter(getContext(), items);

        recyclerView.setAdapter(listAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        shopitemName=view.findViewById(R.id.name);
        shopitemAmount=view.findViewById(R.id.amount);

        okButton=view.findViewById(R.id.okbutton);
        okButton.setOnClickListener(this);

        submitbutton=view.findViewById(R.id.fertigbutton);
        submitbutton.setOnClickListener(this);

        Log.d(TAG, "setRecyclerView: finished");
    }

    public void onClick(View v){
        switch(v.getId()) {
            case R.id.okbutton:
            if (shopitemName.getText().toString().trim().length() <= 0) {
                Toast.makeText(getActivity(), "Please enter a name", Toast.LENGTH_SHORT).show();
                break;
            } else if (shopitemAmount.getText().toString().trim().length() <= 0) {
                Toast.makeText(getActivity(), "Please enter a quantity", Toast.LENGTH_SHORT).show();
                break;
            } else if (Integer.parseInt(shopitemAmount.getText().toString()) < 0) {
                Toast.makeText(getActivity(), "Invalid amount", Toast.LENGTH_SHORT).show();
                break;
            }
            ItemDao itemDao = itemDatabase.itemDao();

            if (shopitemName.getText().toString().equals(itemDao.findItemName(shopitemName.getText().toString()))) {
                Item item = itemDao.getItem(shopitemName.getText().toString());

                item.updateShopAmount(Integer.parseInt(shopitemAmount.getText().toString()));

                updateItemDb(item);

                Toast.makeText(getActivity(), "ShopAmount has been updated", Toast.LENGTH_SHORT).show();

                shopitemName.getText().clear();
                shopitemAmount.getText().clear();

            }
            else {
                safeItemToDb();

                Toast.makeText(getActivity(), "Item added to Shopping_list", Toast.LENGTH_SHORT).show();

                shopitemName.getText().clear();
                shopitemAmount.getText().clear();
            }
            refresh();
            break;

            case R.id.fertigbutton:
                refresh();
        }
    }

    //refresh the adapter
    private void refresh() {
        itemsInit();
        listAdapter = new Shop_ListAdapter(getContext(), items);
        recyclerView.setAdapter(listAdapter);
    }

    //executes ItemsInit-Class
    private void itemsInit() {
        new ItemsInit().execute();
    }

    //initializes the databank/fetches the data added earlier
    private void initDb() {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
    }

    //init shop_item-List but only items with shop_amount>0 are fetched and displayed
    private class ItemsInit extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            items = itemDatabase.itemDao().getShopping();
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: ItemsInit " + items.toString());
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

    }

    private void safeItemToDb()
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Item item = new Item();
                        item.setItemName(shopitemName.getText().toString());
                        item.setShop_amount(Integer.parseInt(shopitemAmount.getText().toString()));

                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.insert(item);

                        List<Item> itemList = itemDao.getAllWords();
                        ArrayList<Item> itemArrayList = new ArrayList<>(itemList);

                        for (Item tempItem : itemArrayList) {
                            Log.i("ShoppingFragment", tempItem.toString());
                        }
                    }
                }
        ).start();
    }

    private void updateItemDb(final Item item)
    {
        new Thread(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

}