package com.example.virtustorage;

import android.arch.lifecycle.Observer;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DietitianFragment extends Fragment {

    public List<Item> items;
    private RecyclerView recyclerView;
    private Dietitian_RecyclerAdapter recyclerAdapter;
    private Dietitian_ListAdapter listAdapter;
    private ItemDatabase itemDatabase;
    private SearchView searchView;
    private ListView listView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDb();
        itemsInit();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.dietitian_fragment, container, false);

        setRecyclerView(view);
        setListView(view);
        setSearchView(view);

        listView.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void setSearchView(View view) {
        searchView = view.findViewById(R.id.searchView);
        searchView.bringToFront();
        listView.bringToFront();
        searchView.setIconifiedByDefault(true); // set the default or resting state of the search field
        searchView.setQueryHint("Search Item"); // set the hint text to display in the query text field
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(onQueryTextListener);


    }

    private void setListView(final View view) {

        listAdapter = new Dietitian_ListAdapter(getContext(), items);

        listView = view.findViewById(R.id.itemListView);

        listView.setTextFilterEnabled(false);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ItemDao itemDao = itemDatabase.itemDao();

                String current_product = listView.getItemAtPosition(position).toString();

                String[] arrayString;

                //Filtert den Namen aus dem String current_product
                arrayString = current_product.split(" Anzahl");

                String name = arrayString[0];

                name = name.substring(name.indexOf("Name: ") + 6);

                Item item = itemDao.getItem(name);

                if (item != null) {
                    item.setIn_dietitian(true);
                }

                updateItemDb(item);

                recyclerAdapter = new Dietitian_RecyclerAdapter(getActivity(), items);
                recyclerView.setAdapter(recyclerAdapter);

                searchView.onActionViewCollapsed();
                listView.setVisibility(View.GONE);
                recyclerView.bringToFront();

                refresh();

            }
        });

    }


    private void updateItemDb(final Item item) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        ItemDao itemDao = itemDatabase.itemDao();
                        itemDao.update(item);
                    }
                }
        ).start();
    }

    //creates the RecyclerView
    private void setRecyclerView(View view) {

        recyclerView = view.findViewById(R.id.recycler_view_dietitian);

        recyclerAdapter = new Dietitian_RecyclerAdapter(getContext(), items);

        recyclerView.setAdapter(recyclerAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);


        swipeActions(recyclerView);
    }

    private void refresh() {
        itemsInit();
        recyclerAdapter = new Dietitian_RecyclerAdapter(getContext(), items);
        recyclerView.setAdapter(recyclerAdapter);
    }

    //initializes the databank/fetches the date added earlier
    private void initDb() {
        itemDatabase = ItemDatabase.getDatabase(getActivity());
    }

    private void itemsInit() {
        new ItemsInit().execute();
    }

    private class ItemsInit extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            items = itemDatabase.itemDao().getDietitianItems();

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

    }

    private SearchView.OnQueryTextListener onQueryTextListener =
            new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    listView.setVisibility(View.VISIBLE);
                    getItemFromDb(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    listView.setVisibility(View.VISIBLE);
                    listView.bringToFront();

                    if (newText.length() == 0) {
                        listAdapter.filter(newText);
                        return true;
                    }
                    getItemFromDb(newText);

                    return true;
                }

                private void getItemFromDb(String searchText) {
                    ItemDatabase.getItemListInfo(getActivity(), searchText)
                            .observe(getActivity(), new Observer<List<Item>>() {
                                @Override
                                public void onChanged(@Nullable List<Item> items) {
                                    if (items == null) {
                                        return;
                                    }

                                    listAdapter = new Dietitian_ListAdapter(getActivity(), items);
                                    listView.setAdapter(listAdapter);
                                }

                            });
                }
            };

    //Implementation and Handling of SwipeActions of StorageList-Items
    private void swipeActions(final RecyclerView recyclerView) {

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT) {

            //this is for drag&drop stuff, which we do not use
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            //here swipes are handled
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Item item = listAdapter.getItem(viewHolder.getAdapterPosition());

                //Swipe to the Left
                if (direction == ItemTouchHelper.LEFT) {
                    recyclerAdapter.removeItem(viewHolder.getAdapterPosition());

                    item.setIn_dietitian(false);
                    updateItemDb(item);

                    //snackbar appears and tells the user what happened
                    Snackbar snackbar = Snackbar.make(recyclerView, "Produkt entfernt!", Snackbar.LENGTH_SHORT);

                    snackbar.show();

                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


}
